﻿namespace Exercise1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.footballButton = new System.Windows.Forms.Button();
            this.questionLabel = new System.Windows.Forms.Label();
            this.factLabel = new System.Windows.Forms.Label();
            this.randomFactLabel = new System.Windows.Forms.Label();
            this.soccerButton = new System.Windows.Forms.Button();
            this.baseballButton = new System.Windows.Forms.Button();
            this.backButton = new System.Windows.Forms.Button();
            this.hockeyButton = new System.Windows.Forms.Button();
            this.basketballButton = new System.Windows.Forms.Button();
            this.tennisButton = new System.Windows.Forms.Button();
            this.quitButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // footballButton
            // 
            this.footballButton.Location = new System.Drawing.Point(12, 201);
            this.footballButton.Name = "footballButton";
            this.footballButton.Size = new System.Drawing.Size(75, 23);
            this.footballButton.TabIndex = 0;
            this.footballButton.Text = "Football";
            this.footballButton.UseVisualStyleBackColor = true;
            this.footballButton.Click += new System.EventHandler(this.FootballButton_Click);
            // 
            // questionLabel
            // 
            this.questionLabel.AutoSize = true;
            this.questionLabel.Location = new System.Drawing.Point(116, 9);
            this.questionLabel.Name = "questionLabel";
            this.questionLabel.Size = new System.Drawing.Size(136, 13);
            this.questionLabel.TabIndex = 1;
            this.questionLabel.Text = "What is your favorite sport?";
            this.questionLabel.UseMnemonic = false;
            // 
            // factLabel
            // 
            this.factLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.factLabel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.factLabel.Location = new System.Drawing.Point(12, 102);
            this.factLabel.Name = "factLabel";
            this.factLabel.Size = new System.Drawing.Size(359, 85);
            this.factLabel.TabIndex = 2;
            this.factLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // randomFactLabel
            // 
            this.randomFactLabel.AutoSize = true;
            this.randomFactLabel.Location = new System.Drawing.Point(92, 71);
            this.randomFactLabel.Name = "randomFactLabel";
            this.randomFactLabel.Size = new System.Drawing.Size(186, 13);
            this.randomFactLabel.TabIndex = 3;
            this.randomFactLabel.Text = "Here is a random fact about this sport!";
            this.randomFactLabel.Visible = false;
            // 
            // soccerButton
            // 
            this.soccerButton.Location = new System.Drawing.Point(12, 230);
            this.soccerButton.Name = "soccerButton";
            this.soccerButton.Size = new System.Drawing.Size(75, 23);
            this.soccerButton.TabIndex = 4;
            this.soccerButton.Text = "Soccer";
            this.soccerButton.UseVisualStyleBackColor = true;
            this.soccerButton.Click += new System.EventHandler(this.SoccerButton_Click);
            // 
            // baseballButton
            // 
            this.baseballButton.Location = new System.Drawing.Point(12, 259);
            this.baseballButton.Name = "baseballButton";
            this.baseballButton.Size = new System.Drawing.Size(75, 23);
            this.baseballButton.TabIndex = 5;
            this.baseballButton.Text = "Baseball";
            this.baseballButton.UseVisualStyleBackColor = true;
            this.baseballButton.Click += new System.EventHandler(this.BaseballButton_Click);
            // 
            // backButton
            // 
            this.backButton.Location = new System.Drawing.Point(12, 310);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(75, 23);
            this.backButton.TabIndex = 6;
            this.backButton.Text = "Back";
            this.backButton.UseVisualStyleBackColor = true;
            this.backButton.Visible = false;
            this.backButton.Click += new System.EventHandler(this.BackButton_Click);
            // 
            // hockeyButton
            // 
            this.hockeyButton.Location = new System.Drawing.Point(296, 201);
            this.hockeyButton.Name = "hockeyButton";
            this.hockeyButton.Size = new System.Drawing.Size(75, 23);
            this.hockeyButton.TabIndex = 7;
            this.hockeyButton.Text = "Hockey";
            this.hockeyButton.UseVisualStyleBackColor = true;
            this.hockeyButton.Click += new System.EventHandler(this.HockeyButton_Click);
            // 
            // basketballButton
            // 
            this.basketballButton.Location = new System.Drawing.Point(296, 230);
            this.basketballButton.Name = "basketballButton";
            this.basketballButton.Size = new System.Drawing.Size(75, 23);
            this.basketballButton.TabIndex = 8;
            this.basketballButton.Text = "Basketball";
            this.basketballButton.UseVisualStyleBackColor = true;
            this.basketballButton.Click += new System.EventHandler(this.BasketballButton_Click);
            // 
            // tennisButton
            // 
            this.tennisButton.Location = new System.Drawing.Point(296, 259);
            this.tennisButton.Name = "tennisButton";
            this.tennisButton.Size = new System.Drawing.Size(75, 23);
            this.tennisButton.TabIndex = 9;
            this.tennisButton.Text = "Tennis";
            this.tennisButton.UseVisualStyleBackColor = true;
            this.tennisButton.Click += new System.EventHandler(this.TennisButton_Click);
            // 
            // quitButton
            // 
            this.quitButton.Location = new System.Drawing.Point(296, 310);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(75, 23);
            this.quitButton.TabIndex = 10;
            this.quitButton.Text = "Quit";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.QuitButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 345);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.tennisButton);
            this.Controls.Add(this.basketballButton);
            this.Controls.Add(this.hockeyButton);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.baseballButton);
            this.Controls.Add(this.soccerButton);
            this.Controls.Add(this.randomFactLabel);
            this.Controls.Add(this.factLabel);
            this.Controls.Add(this.questionLabel);
            this.Controls.Add(this.footballButton);
            this.Name = "Form1";
            this.Text = "Sports Trivia";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button footballButton;
        private System.Windows.Forms.Label questionLabel;
        private System.Windows.Forms.Label factLabel;
        private System.Windows.Forms.Label randomFactLabel;
        private System.Windows.Forms.Button soccerButton;
        private System.Windows.Forms.Button baseballButton;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button hockeyButton;
        private System.Windows.Forms.Button basketballButton;
        private System.Windows.Forms.Button tennisButton;
        private System.Windows.Forms.Button quitButton;
    }
}

