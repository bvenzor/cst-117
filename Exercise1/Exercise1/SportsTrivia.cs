﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void FootballButton_Click(object sender, EventArgs e)
        {
            /*Making Buttons invisible and visible*/
            footballButton.Visible = false;
            soccerButton.Visible = false;
            baseballButton.Visible = false;
            hockeyButton.Visible = false;
            basketballButton.Visible = false;
            tennisButton.Visible = false;
            backButton.Visible = true;
            randomFactLabel.Visible = true;
            questionLabel.Visible = false;

            /*Football Fact*/
            factLabel.Text = "The oldest franchise in professional football is the current Arizona Cardinals!"; 
        }

        private void SoccerButton_Click(object sender, EventArgs e)
        {
            /*Making Buttons invisible and visible*/
            footballButton.Visible = false;
            soccerButton.Visible = false;
            baseballButton.Visible = false;
            hockeyButton.Visible = false;
            basketballButton.Visible = false;
            tennisButton.Visible = false;
            backButton.Visible = true;
            randomFactLabel.Visible = true;
            questionLabel.Visible = false;

            /*Soccer Fact*/
            factLabel.Text = "Manchester United were the first ever FA Premier League winners in the 1992-1993 season!";
        }

        private void BaseballButton_Click(object sender, EventArgs e)
        {
            /*Making Buttons invisible and visible*/
            footballButton.Visible = false;
            soccerButton.Visible = false;
            baseballButton.Visible = false;
            hockeyButton.Visible = false;
            basketballButton.Visible = false;
            tennisButton.Visible = false;
            backButton.Visible = true;
            randomFactLabel.Visible = true;
            questionLabel.Visible = false;

            /*Baseball Fact*/
            factLabel.Text = "Babe Ruth hit his first career home run against the New York Yankees on May 6, 1915!";
        }

        private void HockeyButton_Click(object sender, EventArgs e)
        {
            /*Making Buttons invisible and visible*/
            footballButton.Visible = false;
            soccerButton.Visible = false;
            baseballButton.Visible = false;
            hockeyButton.Visible = false;
            basketballButton.Visible = false;
            tennisButton.Visible = false;
            backButton.Visible = true;
            randomFactLabel.Visible = true;
            questionLabel.Visible = false;

            /*Hockey Fact*/
            factLabel.Text = "The Ottawa Senators were the first NHL team to win back-to-back Stanley Cups in 1921!";
        }

        private void BasketballButton_Click(object sender, EventArgs e)
        {
            /*Making Buttons invisible and visible*/
            footballButton.Visible = false;
            soccerButton.Visible = false;
            baseballButton.Visible = false;
            hockeyButton.Visible = false;
            basketballButton.Visible = false;
            tennisButton.Visible = false;
            backButton.Visible = true;
            randomFactLabel.Visible = true;
            questionLabel.Visible = false;

            /*Basketball Fact*/
            factLabel.Text = "The Los Angeles Lakers own the longest winning streak in NBA history at 33 (as of 2019)!";
        }

        private void TennisButton_Click(object sender, EventArgs e)
        {
            /*Making Buttons invisible and visible*/
            footballButton.Visible = false;
            soccerButton.Visible = false;
            baseballButton.Visible = false;
            hockeyButton.Visible = false;
            basketballButton.Visible = false;
            tennisButton.Visible = false;
            backButton.Visible = true;
            randomFactLabel.Visible = true;
            questionLabel.Visible = false;

            /*Tennis Fact*/
            factLabel.Text = "The longest tennis match in history was 11 hours and 5 minutes, in 2010!";
        }

        private void BackButton_Click(object sender, EventArgs e)
        {
            /*Making Buttons invisible and visible*/
            footballButton.Visible = true;
            soccerButton.Visible = true;
            baseballButton.Visible = true;
            hockeyButton.Visible = true;
            basketballButton.Visible = true;
            tennisButton.Visible = true;
            backButton.Visible = false;
            randomFactLabel.Visible = false;
            questionLabel.Visible = true;
            /*No Fact*/
            factLabel.Text = "";
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
