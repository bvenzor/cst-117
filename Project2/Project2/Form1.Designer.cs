﻿namespace Project2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.findSongButton = new System.Windows.Forms.Button();
            this.quitButton = new System.Windows.Forms.Button();
            this.yearBox = new System.Windows.Forms.ListBox();
            this.selectYearLabel = new System.Windows.Forms.Label();
            this.selectSourceLabel = new System.Windows.Forms.Label();
            this.billboardButton = new System.Windows.Forms.RadioButton();
            this.officialButton = new System.Windows.Forms.RadioButton();
            this.sourceCheckBox = new System.Windows.Forms.CheckBox();
            this.selectDetailsLabel = new System.Windows.Forms.Label();
            this.yearCheckBox = new System.Windows.Forms.CheckBox();
            this.showSongLabel = new System.Windows.Forms.Label();
            this.songLabel = new System.Windows.Forms.Label();
            this.showSourceLabel = new System.Windows.Forms.Label();
            this.showYearLabel = new System.Windows.Forms.Label();
            this.errorLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // findSongButton
            // 
            this.findSongButton.Location = new System.Drawing.Point(227, 305);
            this.findSongButton.Name = "findSongButton";
            this.findSongButton.Size = new System.Drawing.Size(75, 23);
            this.findSongButton.TabIndex = 0;
            this.findSongButton.Text = "Find Song";
            this.findSongButton.UseVisualStyleBackColor = true;
            this.findSongButton.Click += new System.EventHandler(this.FindSongButton_Click);
            // 
            // quitButton
            // 
            this.quitButton.Location = new System.Drawing.Point(329, 305);
            this.quitButton.Name = "quitButton";
            this.quitButton.Size = new System.Drawing.Size(75, 23);
            this.quitButton.TabIndex = 1;
            this.quitButton.Text = "Quit";
            this.quitButton.UseVisualStyleBackColor = true;
            this.quitButton.Click += new System.EventHandler(this.QuitButton_Click);
            // 
            // yearBox
            // 
            this.yearBox.FormattingEnabled = true;
            this.yearBox.Items.AddRange(new object[] {
            "2018",
            "2017",
            "2016",
            "2015",
            "2014",
            "2013",
            "2012",
            "2011",
            "2010"});
            this.yearBox.Location = new System.Drawing.Point(12, 25);
            this.yearBox.Name = "yearBox";
            this.yearBox.Size = new System.Drawing.Size(120, 95);
            this.yearBox.TabIndex = 2;
            // 
            // selectYearLabel
            // 
            this.selectYearLabel.AutoSize = true;
            this.selectYearLabel.Location = new System.Drawing.Point(9, 9);
            this.selectYearLabel.Name = "selectYearLabel";
            this.selectYearLabel.Size = new System.Drawing.Size(63, 13);
            this.selectYearLabel.TabIndex = 3;
            this.selectYearLabel.Text = "Select year:";
            // 
            // selectSourceLabel
            // 
            this.selectSourceLabel.AutoSize = true;
            this.selectSourceLabel.Location = new System.Drawing.Point(9, 140);
            this.selectSourceLabel.Name = "selectSourceLabel";
            this.selectSourceLabel.Size = new System.Drawing.Size(75, 13);
            this.selectSourceLabel.TabIndex = 4;
            this.selectSourceLabel.Text = "Select source:";
            // 
            // billboardButton
            // 
            this.billboardButton.AutoSize = true;
            this.billboardButton.Location = new System.Drawing.Point(24, 165);
            this.billboardButton.Name = "billboardButton";
            this.billboardButton.Size = new System.Drawing.Size(108, 17);
            this.billboardButton.TabIndex = 5;
            this.billboardButton.TabStop = true;
            this.billboardButton.Text = "Billboard Top 100";
            this.billboardButton.UseVisualStyleBackColor = true;
            // 
            // officialButton
            // 
            this.officialButton.AutoSize = true;
            this.officialButton.Location = new System.Drawing.Point(24, 188);
            this.officialButton.Name = "officialButton";
            this.officialButton.Size = new System.Drawing.Size(85, 17);
            this.officialButton.TabIndex = 6;
            this.officialButton.TabStop = true;
            this.officialButton.Text = "Official Chart";
            this.officialButton.UseVisualStyleBackColor = true;
            // 
            // sourceCheckBox
            // 
            this.sourceCheckBox.AutoSize = true;
            this.sourceCheckBox.Location = new System.Drawing.Point(24, 246);
            this.sourceCheckBox.Name = "sourceCheckBox";
            this.sourceCheckBox.Size = new System.Drawing.Size(60, 17);
            this.sourceCheckBox.TabIndex = 7;
            this.sourceCheckBox.Text = "Source";
            this.sourceCheckBox.UseVisualStyleBackColor = true;
            // 
            // selectDetailsLabel
            // 
            this.selectDetailsLabel.AutoSize = true;
            this.selectDetailsLabel.Location = new System.Drawing.Point(9, 220);
            this.selectDetailsLabel.Name = "selectDetailsLabel";
            this.selectDetailsLabel.Size = new System.Drawing.Size(70, 13);
            this.selectDetailsLabel.TabIndex = 8;
            this.selectDetailsLabel.Text = "Show details:";
            // 
            // yearCheckBox
            // 
            this.yearCheckBox.AutoSize = true;
            this.yearCheckBox.Location = new System.Drawing.Point(24, 269);
            this.yearCheckBox.Name = "yearCheckBox";
            this.yearCheckBox.Size = new System.Drawing.Size(48, 17);
            this.yearCheckBox.TabIndex = 9;
            this.yearCheckBox.Text = "Year";
            this.yearCheckBox.UseVisualStyleBackColor = true;
            // 
            // showSongLabel
            // 
            this.showSongLabel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.showSongLabel.Location = new System.Drawing.Point(261, 58);
            this.showSongLabel.Name = "showSongLabel";
            this.showSongLabel.Size = new System.Drawing.Size(120, 95);
            this.showSongLabel.TabIndex = 10;
            this.showSongLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // songLabel
            // 
            this.songLabel.AutoSize = true;
            this.songLabel.Location = new System.Drawing.Point(304, 25);
            this.songLabel.Name = "songLabel";
            this.songLabel.Size = new System.Drawing.Size(32, 13);
            this.songLabel.TabIndex = 11;
            this.songLabel.Text = "Song";
            // 
            // showSourceLabel
            // 
            this.showSourceLabel.AutoSize = true;
            this.showSourceLabel.Location = new System.Drawing.Point(258, 165);
            this.showSourceLabel.Name = "showSourceLabel";
            this.showSourceLabel.Size = new System.Drawing.Size(0, 13);
            this.showSourceLabel.TabIndex = 12;
            this.showSourceLabel.Visible = false;
            // 
            // showYearLabel
            // 
            this.showYearLabel.AutoSize = true;
            this.showYearLabel.Location = new System.Drawing.Point(258, 190);
            this.showYearLabel.Name = "showYearLabel";
            this.showYearLabel.Size = new System.Drawing.Size(0, 13);
            this.showYearLabel.TabIndex = 13;
            this.showYearLabel.Visible = false;
            // 
            // errorLabel
            // 
            this.errorLabel.Location = new System.Drawing.Point(138, 25);
            this.errorLabel.Name = "errorLabel";
            this.errorLabel.Size = new System.Drawing.Size(117, 68);
            this.errorLabel.TabIndex = 14;
            this.errorLabel.Text = "Please make sure you select a year AND a source!";
            this.errorLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.errorLabel.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 340);
            this.Controls.Add(this.errorLabel);
            this.Controls.Add(this.showYearLabel);
            this.Controls.Add(this.showSourceLabel);
            this.Controls.Add(this.songLabel);
            this.Controls.Add(this.showSongLabel);
            this.Controls.Add(this.yearCheckBox);
            this.Controls.Add(this.selectDetailsLabel);
            this.Controls.Add(this.sourceCheckBox);
            this.Controls.Add(this.officialButton);
            this.Controls.Add(this.billboardButton);
            this.Controls.Add(this.selectSourceLabel);
            this.Controls.Add(this.selectYearLabel);
            this.Controls.Add(this.yearBox);
            this.Controls.Add(this.quitButton);
            this.Controls.Add(this.findSongButton);
            this.Name = "Form1";
            this.Text = "Chart-Topping Song Finder";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button findSongButton;
        private System.Windows.Forms.Button quitButton;
        private System.Windows.Forms.ListBox yearBox;
        private System.Windows.Forms.Label selectYearLabel;
        private System.Windows.Forms.Label selectSourceLabel;
        private System.Windows.Forms.RadioButton billboardButton;
        private System.Windows.Forms.RadioButton officialButton;
        private System.Windows.Forms.CheckBox sourceCheckBox;
        private System.Windows.Forms.Label selectDetailsLabel;
        private System.Windows.Forms.CheckBox yearCheckBox;
        private System.Windows.Forms.Label showSongLabel;
        private System.Windows.Forms.Label songLabel;
        private System.Windows.Forms.Label showSourceLabel;
        private System.Windows.Forms.Label showYearLabel;
        private System.Windows.Forms.Label errorLabel;
    }
}

