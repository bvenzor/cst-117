﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void FindSongButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Make errorLabel disappear
                errorLabel.Visible = false;

                // Hold the ListBox Selection
                string year;

                // Billboard list
                if (yearBox.SelectedIndex != -1 && billboardButton.Checked == true && officialButton.Checked == false)
                {
                    year = yearBox.SelectedItem.ToString();

                    // Showing source
                    if (sourceCheckBox.Checked == true)
                    {
                        showSourceLabel.Visible = true;
                        showSourceLabel.Text = "Billboard Hot 100";
                    }

                    switch (year)
                    {
                        case "2018":
                            showSongLabel.Text = "God's Plan by: Drake";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2018";
                            }
                            break;
                        case "2017":
                            showSongLabel.Text = "Shape of You by: Ed Sheeran";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2017";
                            }
                            break;
                        case "2016":
                            showSongLabel.Text = "Love Yourself by: Justin Bieber";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2016";
                            }
                            break;
                        case "2015":
                            showSongLabel.Text = "Uptown Funk by: Mark Ronson, featuring Bruno Mars";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2015";
                            }
                            break;
                        case "2014":
                            showSongLabel.Text = "Happy by: Pharrell Williams";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2014";
                            }
                            break;
                        case "2013":
                            showSongLabel.Text = "Thrift Shop by: Macklemore & Ryan Lewis, featuring Wanz";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2013";
                            }
                            break;
                        case "2012":
                            showSongLabel.Text = "Somebody That I Used To Know by: Gotye, featuring Kimbra";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2012";
                            }
                            break;
                        case "2011":
                            showSongLabel.Text = "Rolling In The Deep by: Adele";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2011";
                            }
                            break;
                        case "2010":
                            showSongLabel.Text = "Tik Tok by: Kesha";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2010";
                            }
                            break;
                    }

                }
                // Official list
                if (yearBox.SelectedIndex != -1 && officialButton.Checked == true && billboardButton.Checked == false)
                {
                    year = yearBox.SelectedItem.ToString();

                    // Showing source
                    if (sourceCheckBox.Checked == true)
                    {
                        showSourceLabel.Visible = true;
                        showSourceLabel.Text = "The Official Chart";
                    }

                    switch (year)
                    {
                        case "2018":
                            showSongLabel.Text = "One Kiss by: Calvin Harris and Dua Lipa";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2018";
                            }
                            break;
                        case "2017":
                            showSongLabel.Text = "Shape of You by: Ed Sheeran";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2017";
                            }
                            break;
                        case "2016":
                            showSongLabel.Text = "One Dance by: Drake, featuring Wizkid and Kyla";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2016";
                            }
                            break;
                        case "2015":
                            showSongLabel.Text = "Uptown Funk by: Mark Ronson, featuring Bruno Mars";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2015";
                            }
                            break;
                        case "2014":
                            showSongLabel.Text = "Happy by: Pharrell Williams";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2014";
                            }
                            break;
                        case "2013":
                            showSongLabel.Text = "Blurred Lines by: Robin Thicke, featuring T.I. and Pharrell Wiliams";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2013";
                            }
                            break;
                        case "2012":
                            showSongLabel.Text = "Somebody That I Used To Know by: Gotye, featuring Kimbra";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2012";
                            }
                            break;
                        case "2011":
                            showSongLabel.Text = "Someone Like You by: Adele";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2011";
                            }
                            break;
                        case "2010":
                            showSongLabel.Text = "Love The Way You Lie by: Eminem, featuring Rihanna";
                            if (yearCheckBox.Checked == true)
                            {
                                showYearLabel.Visible = true;
                                showYearLabel.Text = "2010";
                            }
                            break;
                    }
                }

                if (yearBox.SelectedIndex != -1 && billboardButton.Checked == false && officialButton.Checked == false)
                {
                    // Error
                    errorLabel.Visible = true;
                }

                if (yearBox.SelectedIndex == -1)
                {
                    //Error
                    errorLabel.Visible = true;
                }

                if (yearCheckBox.Checked == false)
                {
                    // Making year disappear
                    showYearLabel.Visible = false;
                }
                if (sourceCheckBox.Checked == false)
                {
                    // making Source disappear
                    showSourceLabel.Visible = false;
                }
            }

            catch
            {
                //Error
                errorLabel.Visible = true;
            }
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
    

